# Jump Start
A starter template for observable high-performance web services.


## Details
Includes tracing, metrics, app configuration, data storage, timeout, route fallback and graceful shutdown by leveraging:

- [anyhow](https://crates.io/crates/anyhow) - *"a trait object based error type for easy idiomatic error handling"*
- [axum](https://crates.io/crates/axum) - *"web application framework that focuses on ergonomics and modularity"*
- [config](https://crates.io/crates/config) - *"Layered configuration system for Rust applications"*
- [hyper](https://crates.io/crates/hyper) - *"fast and correct HTTP implementation"*
- [metrics](https://crates.io/crates/metrics) - *"lightweight metrics facade"*
- [serde](https://crates.io/crates/serde) - *"framework for serializing and deserializing data structures efficiently and generically"*
- [sqlx](https://crates.io/crates/sqlx) - *"async, pure Rust SQL crate featuring compile-time checked queries without a DSL"*
- [tokio](https://crates.io/crates/tokio) - *"runtime for writing reliable network applications without compromising speed"*
- [tower](https://crates.io/crates/tower) - *"library of modular and reusable components for building robust networking clients and servers"*
- [tracing](https://crates.io/crates/tracing) - *"application-level tracing"*


For further details look at `Cargo.toml` or `CHANGELOG.md`.

## Getting Started
```bash
# Create tmp directory
# This is where databases and logs will be stored
mkdir -p tmp/test

# Install sqlx-cli
cargo install sqlx-cli --no-default-features --features rustls,sqlite

# Set up database
sqlx database create
sqlx migrate run

# Run the app
cargo run

# Run tests
cargo test
```

## Comments
- Exposes a prometheus metrics endpoint at "/metrics"
- Writes Uuid-tagged tracing logs to stdout + file
- Uses SQLite, but sqlx can handle other engines as well
- Handles timeouts and fallback
