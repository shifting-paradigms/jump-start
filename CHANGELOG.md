# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2022-07-11
### Added
- Database can be mocked by injecting database trait object

## [0.1.3] - 2022-05-15
### Fixed
- tracing output exposes post data
- request id appears without field name in tracing output
- base config contains old log file name

## [0.1.0] - 2022-05-03
### Added
- Basic healthcheck
- Handling of timeouts
- 404 fallback
- Tracing log to stdout
- Tracing log to specified file
- Exposure of prometheus metrics endpoint at "/metrics"
- Sqlite data storage
- Graceful shutdown
