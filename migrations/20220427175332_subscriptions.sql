-- Create Subscriptions Table
CREATE TABLE IF NOT EXISTS subscriptions(
    id TEXT NOT NULL PRIMARY KEY,
    email TEXT NOT NULL UNIQUE,
    reachability TEXT NOT NULL,
    has_accepted_terms INTEGER NOT NULL,
    is_confirmed INTEGER DEFAULT 0,
    subscribed_at TEXT NOT NULL
);
