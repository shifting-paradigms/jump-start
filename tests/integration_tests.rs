use axum::{
    body::Body,
    http::{header, Method, Request, StatusCode},
};
use jump_start::{database::DynDatabaseQueries, startup};
use std::sync::Arc;
use tower::ServiceExt; // for `app.oneshot()`

mod common;

#[tokio::test]
async fn server_is_alive() {
    let name = uuid::Uuid::new_v4().to_string();
    let db = common::create_test_db(&name).await;
    let dyn_db = Arc::new(db) as DynDatabaseQueries;
    let app = startup::create_app(dyn_db);
    let response = app
        .oneshot(
            Request::builder()
                .uri("/heartbeat")
                .body(Body::empty())
                .expect("Failed to create request"),
        )
        .await
        .expect("Failed to execute oneshot");

    assert_eq!(response.status(), StatusCode::OK);
    common::remove_test_db(&name).await;
}

#[tokio::test]
async fn create_subscription_returns_payload() {
    let name = uuid::Uuid::new_v4().to_string();
    let db = common::create_test_db(&name).await;
    let dyn_db = Arc::new(db) as DynDatabaseQueries;
    let app = startup::create_app(dyn_db);
    let req_body = format!("{{\"email\":\"peterparker@spidy.com\",\"has_accepted_terms\":true}}");
    let response = app
        .oneshot(
            Request::builder()
                .method(Method::POST)
                .uri("/subscriptions")
                .header(header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
                .body(Body::from(req_body.clone()))
                .expect("Failed to create json body"),
        )
        .await
        .expect("Failed to execute request");
    assert_eq!(response.status(), StatusCode::CREATED);
    let res_body = hyper::body::to_bytes(response.into_body())
        .await
        .expect("Failed to parse response body");
    assert_eq!(req_body.as_bytes(), res_body);
    common::remove_test_db(&name).await;
}

#[tokio::test]
async fn create_subscription_returns_400_for_missing_data() {
    let name = uuid::Uuid::new_v4().to_string();
    let db = common::create_test_db(&name).await;
    let dyn_db = Arc::new(db) as DynDatabaseQueries;
    let app = startup::create_app(dyn_db);
    let response = app
        .oneshot(
            Request::builder()
                .method(Method::POST)
                .uri("/subscriptions")
                .header(header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
                .body(Body::from("".to_string()))
                .expect("Failed to create json body"),
        )
        .await
        .expect("Failed to execute request");

    assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    common::remove_test_db(&name).await;
}
