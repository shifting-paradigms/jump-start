use sqlx::migrate::{MigrateDatabase, Migrator};
use sqlx::sqlite::SqlitePool;

pub async fn create_test_db(name: &str) -> SqlitePool {
    let uri = format!("sqlite:tmp/test/{}.db", name);
    let _db = sqlx::Sqlite::create_database(&uri)
        .await
        .expect("failed to create database");
    let pool = SqlitePool::connect(&uri)
        .await
        .expect("failed to connect to database pool");

    let m = Migrator::new(std::path::Path::new("./migrations"))
        .await
        .expect("failed to create migrator");
    m.run(&pool).await.expect("failed to run migrations");
    pool
}

pub async fn remove_test_db(name: &str) {
    let suffixes = vec!["", "-shm", "-wal"];

    for suffix in suffixes {
        std::fs::remove_file(&format!("tmp/test/{}.db{}", name, suffix))
            .expect("failed to remove database");
    }
}
