use crate::database::DynDatabaseQueries;
use axum::{http::StatusCode, response::IntoResponse, Extension, Json};
use serde::{Deserialize, Serialize};

#[tracing::instrument(skip_all)]
pub async fn create_subscription(
    Extension(_pool): Extension<DynDatabaseQueries>,
    Json(subscribe_request): Json<SubscriptionFormData>,
) -> impl IntoResponse {
    (StatusCode::CREATED, Json(subscribe_request))
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SubscriptionFormData {
    pub email: String,
    pub has_accepted_terms: bool,
}
