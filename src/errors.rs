use axum::{
    body::Body,
    http::{Request, StatusCode},
    response::IntoResponse,
    BoxError,
};

#[tracing::instrument(skip_all)]
pub async fn handle_timeout_error(err: BoxError) -> StatusCode {
    if err.is::<tower::timeout::error::Elapsed>() {
        tracing::error!("request timeout exceeded");
        StatusCode::REQUEST_TIMEOUT
    } else {
        tracing::error!("unhandled internal error: {}", err);
        StatusCode::INTERNAL_SERVER_ERROR
    }
}

#[tracing::instrument(skip_all, fields(method = %req.method(), uri = %req.uri()))]
pub async fn handle_404(req: Request<Body>) -> impl IntoResponse {
    tracing::warn!("received suspicious request");
    StatusCode::NOT_FOUND
}
