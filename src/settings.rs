use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;
use std::env;

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct App {
    host: String,
    port: u16,
}

impl App {
    pub fn address(self) -> String {
        format!("{}:{}", self.host, self.port)
    }
}

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Database {
    path: String,
    name: String,
}

impl Database {
    pub fn url(self) -> String {
        format!("sqlite:{}/{}.db", self.path, self.name)
    }
}

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Log {
    pub level: String,
    pub path: String,
    pub file_name: String,
}

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Settings {
    pub app: App,
    pub database: Database,
    pub log: Log,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let app_environment = env::var("APP_ENV").unwrap_or_else(|_| "development".into());

        let settings = Config::builder()
            .add_source(File::with_name("config/base"))
            .add_source(File::with_name(&format!("config/{}", app_environment)).required(false))
            // Add an optional local configuration
            // This file shouldn't be checked in to git
            .add_source(File::with_name("config/local").required(false))
            // Eg.. APP_APP_PORT=8080 APP_LOG_LEVEL=info
            .add_source(Environment::with_prefix("app").separator("_"))
            .build()
            .expect("failed to build config");

        settings.try_deserialize()
    }
}
