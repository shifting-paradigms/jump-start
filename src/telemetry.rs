use crate::settings::Log;
use axum::{
    body::{Body, Bytes},
    extract::MatchedPath,
    http::HeaderMap,
    http::Request,
    middleware,
    middleware::Next,
    response::IntoResponse,
    response::Response,
    routing::get,
    Router,
};
use metrics_exporter_prometheus::{BuildError, Matcher, PrometheusBuilder, PrometheusHandle};
use std::{future::ready, time::Duration, time::Instant};
use tower_http::{classify::ServerErrorsFailureClass, trace::TraceLayer};
use tracing::{subscriber::set_global_default, Span, Subscriber};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_log::LogTracer;
use tracing_subscriber::{fmt, layer::SubscriberExt, EnvFilter, Registry};

pub fn get_tracing_filter(level: &str) -> EnvFilter {
    EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new(level))
}

pub fn get_subscriber(log_config: &Log) -> (impl Subscriber + Send + Sync, WorkerGuard) {
    let file_appender = tracing_appender::rolling::daily(&log_config.path, &log_config.file_name);
    let (non_blocking, guard) = tracing_appender::non_blocking(file_appender);
    (
        Registry::default()
            .with(get_tracing_filter(&log_config.level))
            .with(fmt::layer().pretty().with_writer(std::io::stdout))
            .with(fmt::layer().json().with_writer(non_blocking)),
        guard,
    )
    // guard is returned only to be scoped in main
}

pub fn init_subscriber(subscriber: impl Subscriber + Send + Sync) {
    LogTracer::init().expect("Failed to set logger");
    set_global_default(subscriber).expect("Failed to set subscriber");
}

pub fn setup_metrics_recorder() -> Result<PrometheusHandle, BuildError> {
    const EXPONENTIAL_SECONDS: &[f64] = &[
        0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1.0, 2.5, 5.0, 10.0, 20.0,
    ];

    let metrics_recorder = PrometheusBuilder::new()
        .set_buckets_for_metric(
            Matcher::Full("http_requests_duration_seconds".to_string()),
            EXPONENTIAL_SECONDS,
        )?
        .install_recorder()?;

    Ok(metrics_recorder)
}

pub async fn track_metrics<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let start = Instant::now();
    let path = if let Some(matched_path) = req.extensions().get::<MatchedPath>() {
        matched_path.as_str().to_owned()
    } else {
        req.uri().path().to_owned()
    };
    let method = req.method().clone();

    let response = next.run(req).await;

    let latency = start.elapsed().as_secs_f64();
    let status = response.status().as_u16().to_string();

    let labels = [
        ("method", method.to_string()),
        ("path", path),
        ("status", status),
    ];

    metrics::increment_counter!("http_requests_total", &labels);
    metrics::histogram!("http_requests_duration_seconds", latency, &labels);

    response
}

pub trait Telemetry {
    fn with_prometheus_metrics(self, metrics_recorder: PrometheusHandle) -> Self;
    fn with_http_trace_layer(self) -> Self;
}

impl Telemetry for Router {
    fn with_prometheus_metrics(self, metrics_recorder: PrometheusHandle) -> Self {
        self.route("/metrics", get(move || ready(metrics_recorder.render())))
            .route_layer(middleware::from_fn(track_metrics))
    }

    fn with_http_trace_layer(self) -> Self {
        self.layer(
            TraceLayer::new_for_http()
                .make_span_with(|_request: &Request<Body>| {
                    tracing::span!(
                        tracing::Level::DEBUG,
                        "http-request",
                        id = uuid::Uuid::new_v4().to_string().as_str(),
                    )
                })
                .on_request(|request: &Request<Body>, _span: &Span| {
                    tracing::debug!("{} {}", request.method(), request.uri().path())
                })
                .on_response(|_response: &Response, latency: Duration, _span: &Span| {
                    tracing::debug!("response generated in {:?}", latency)
                })
                .on_body_chunk(|chunk: &Bytes, _latency: Duration, _span: &Span| {
                    tracing::debug!("sending {} bytes", chunk.len())
                })
                .on_eos(
                    |_trailers: Option<&HeaderMap>, stream_duration: Duration, _span: &Span| {
                        tracing::debug!("stream closed after {:?}", stream_duration)
                    },
                )
                .on_failure(
                    |error: ServerErrorsFailureClass, _latency: Duration, _span: &Span| {
                        tracing::error!("something went wrong: {}", error)
                    },
                ),
        )
    }
}
