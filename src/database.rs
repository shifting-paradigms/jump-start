use sqlx::sqlite::SqlitePool;
use std::sync::Arc;

pub async fn init_db(database_url: &str) -> anyhow::Result<DynDatabaseQueries> {
    let pool = DatabasePool::connect(database_url).await?;
    Ok(Arc::new(pool) as DynDatabaseQueries)
}

pub type DynDatabaseQueries = Arc<dyn DatabaseQueries + Send + Sync>;

#[cfg_attr(test, mockall::automock)]
#[async_trait::async_trait]
pub trait DatabaseQueries {
    async fn insert(&self) -> anyhow::Result<()>;
}

pub type DatabasePool = SqlitePool;

#[async_trait::async_trait]
impl DatabaseQueries for DatabasePool {
    #[tracing::instrument(skip_all)]
    async fn insert(&self) -> anyhow::Result<()> {
        Ok(())
    }
}
