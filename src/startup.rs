use crate::{database::DynDatabaseQueries, errors, routes};
use axum::{
    error_handling::HandleErrorLayer,
    handler::Handler,
    routing::{get, post},
    Extension, Router,
};

#[tracing::instrument(skip_all)]
pub fn create_app(pool: DynDatabaseQueries) -> Router {
    tracing::debug!("creating app instance");
    let app = Router::new()
        .route("/heartbeat", get(|| async {}))
        .route(
            "/subscriptions",
            post(routes::subscriptions::create_subscription),
        )
        .layer(
            tower::ServiceBuilder::new()
                .layer(HandleErrorLayer::new(errors::handle_timeout_error))
                .timeout(std::time::Duration::from_secs(30))
                .layer(Extension(pool)),
        );

    app.fallback(errors::handle_404.into_service())
}
