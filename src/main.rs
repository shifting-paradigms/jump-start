use jump_start::{
    database::DynDatabaseQueries,
    settings, startup,
    telemetry::{
        get_subscriber,
        init_subscriber,
        setup_metrics_recorder,
        Telemetry, // implements telemetry methods for Router
    },
};
use sqlx::sqlite::SqlitePool;
use std::{net::SocketAddr, str::FromStr, sync::Arc};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let config = settings::Settings::new().expect("failed to create config");

    let (subscriber, _guard) = get_subscriber(&config.log);
    init_subscriber(subscriber);

    if std::env::var("APP_ENV").is_err() {
        tracing::warn!("'APP_ENV' is not set");
    }

    let pool = SqlitePool::connect(&config.database.url()).await?;
    let db = Arc::new(pool) as DynDatabaseQueries;

    let metrics_recorder = setup_metrics_recorder().expect("failed to build metrics recorder");
    let app = startup::create_app(db)
        .with_prometheus_metrics(metrics_recorder)
        .with_http_trace_layer();
    let addr = SocketAddr::from_str(&config.app.address()).expect("failed to parse app address");

    tracing::info!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .with_graceful_shutdown(handle_shutdown())
        .await
        .unwrap();
    Ok(())
}

#[tracing::instrument]
async fn handle_shutdown() {
    let ctrl_c = async {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    tracing::info!("termination signal received, starting graceful shutdown");
}
